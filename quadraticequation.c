#include<stdio.h>
#include<math.h>
int main()
{
double a,b,c,discriminant,root1,root2,realpart,imaginarypart;
printf("enter coefficients a,b and c");
scanf("%f%f%f",&a,&b,&c);
discriminant=b*b-4*a*c;
if(discriminant>0)
{
root1=(-b+sqrt(discriminant))/(2*a);
root2=(-b-sqrt(discriminant))/(2*a);
printf("root1=%.21f and root2=%.21f",root1,root2);
}
else if(discriminant==0)
{
root1=root2=-b/(2*a);
printf("root1=root2=%.21f;",root1);
}
else
{
real part=-b/(2*a);
imaginarypart=sqrt(-discriminant)/(2*a);
printf("root1=%.21f+%.21fi and root2=%.2f-%.2fi",realpart,imaginarypart,realpart,imaginarypart);
}
return 0;
}